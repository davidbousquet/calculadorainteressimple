<%-- 
    Document   : index
    Created on : Mar 28, 2021, 1:15:06 PM
    Author     : davidbousquet

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluación unidad 1 - UNI01</title>
    </head>
    <body>
        <h1>Calculadora de Interés Simple</h1>
        <form name="CalculadoraInteresForm" action="CalculadoraInteresController" method="POST">
            <label for="capital">Ingrese capital:</label>
            <input type="number" name="capital" />
            <label for="numAnos">Ingrese número de años</label>
            <input type="number" name="numAnos" />
            <label for="tasaInteresAnual">Ingrese Tasa Interés Anual:</label>
            <input type="number" name="tasaInteresAnual" />
            <button>Calcular</button>
        </form>
        <h4>
            Estudiante: David Bousquet
            Asignatura: TALLER DE APLICACIONES EMPRESARIALES - IC201IECIREOL
        </h4>
    </body>
</html>
