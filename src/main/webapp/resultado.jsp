<%-- 
    Document   : resultado
    Created on : Apr 6, 2021, 9:22:26 PM
    Author     : davidbousquet
--%>

<%@page import="models.CalculadoraInteres"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    CalculadoraInteres calculadoraInteres = (CalculadoraInteres)request.getAttribute("calculadoraInteres");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>Resultado</h1>
        <%
            out.write("<h3>El interés producido es: $" + calculadoraInteres.getInteresSimpleProducido() + "</h3>");
        %>
        <a href="index.jsp"> Volver </a>
        
    </body>
</html>
