/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author davidbousquet
 */
public class CalculadoraInteres {
    
    private int capital;
    private double interesSimpleProducido;
    private double tasaInteresAnual;
    private int numAnos;

    public int getCapital() {
        return capital;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public double getInteresSimpleProducido() {
        return interesSimpleProducido;
    }

    public void setInteresSimpleProducido(double interesSimpleProducido) {
        this.interesSimpleProducido = interesSimpleProducido;
    }

    public double getTasaInteresAnual() {
        return tasaInteresAnual;
    }

    public void setTasaInteresAnual(double tasaInteresAnual) {
        this.tasaInteresAnual = tasaInteresAnual;
    }

    public int getNumAnos() {
        return numAnos;
    }

    public void setNumAnos(int numAnos) {
        this.numAnos = numAnos;
    }
    
    public void Calcular() {
        this.interesSimpleProducido = this.capital * (this.tasaInteresAnual/100) * numAnos;
    }
    
}
