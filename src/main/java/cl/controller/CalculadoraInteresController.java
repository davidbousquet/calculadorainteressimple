/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.CalculadoraInteres;


/**
 *
 * @author davidbousquet
 */
@WebServlet(name = "CalculadoraInteresController", urlPatterns = {"/CalculadoraInteresController"})
public class CalculadoraInteresController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CalculadoraInteresController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CalculadoraInteresController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int capital = Integer.parseInt(request.getParameter("capital"));
        int numAnos = Integer.parseInt(request.getParameter("numAnos"));
        double tasaInteresAnual = Double.parseDouble(request.getParameter("tasaInteresAnual"));

//        if (capital < 0 || numAnos < 0 || tasaInteresAnual < 0) {
//            //todo retornar error de ingreso de datos
//            request.setAttribute("error", "Parámetros erróneos");
//            request.getRequestDispatcher("resultado.jsp").forward(request, response);
//        } else {
        CalculadoraInteres calculadoraInteres = new CalculadoraInteres();
        calculadoraInteres.setCapital(capital);
        calculadoraInteres.setNumAnos(numAnos);
        calculadoraInteres.setTasaInteresAnual(tasaInteresAnual);

        calculadoraInteres.Calcular();

        request.setAttribute("calculadoraInteres", calculadoraInteres);
        request.getRequestDispatcher("resultado.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
